import
  httpclient,
  json,
  strutils,
  sequtils

type
  Turso* = ref object
    url*: string
    token*: string
    client*: HttpClient

proc connect*(url, token: string): Turso =
  ## Connect to the Turso API
  ## url: The URL of the Turso API
  ## token: The token to authenticate with the Turso API
  ## return: A Turso object
  ## Example:
  ## ```var turso = connect("https://api.turso.io", "your-token")```
  
  var
    url1 = url & "/v2/pipeline"
    url2 = url1

  if url1.contains("https"):
    url2 = url1.replace("libsql", "https")
  
  result = Turso(url: url2, token: token, client: newHttpClient())

proc execute*(turso: Turso, sql: string)=
  ## Execute a SQL statement
  ## turso: A Turso object
  ## sql: The SQL statement to execute
  ## Example:
  ## ```turso.execute("SELECT * FROM table")```
  var
    body = %*{
      "requests": [
        { "type": "execute", "stmt": { "sql": sql } },
        { "type": "close" }
      ]
    }

  turso.client.headers = newHttpHeaders({ "Authorization": "Bearer " & turso.token, "Content-Type": "application/json" })

  var _ = turso.client.request(turso.url, httpMethod = HttpPost, body = $body, headers = turso.client.headers)

proc getData*(turso: Turso, sql: string): JsonNode =
  ## Get data from a SQL statement
  ## turso: A Turso object
  ## sql: The SQL statement to execute
  ## return: A JsonNode object
  ## Example:
  ## ```var data = turso.getData("SELECT * FROM table")```
  var
    body = %*{
      "requests": [
        { "type": "execute", "stmt": { "sql": sql } },
        { "type": "close" }
      ]
    }

  turso.client.headers = newHttpHeaders({ "Authorization": "Bearer " & turso.token, "Content-Type": "application/json" })

  var
    response = turso.client.request(turso.url, httpMethod = HttpPost, body = $body, headers = turso.client.headers)
    bod = parseJson(response.body)
  result = bod["results"][0]["response"]["result"]

proc close*(turso: Turso)=
  ## Close the connection to the Turso API
  ## turso: A Turso object
  ## Example:
  ## ```turso.close()```
  turso.client.close()

proc insertId*(turso: Turso, table: string, fields, values: seq[string]): string =
  ## Insert a row into a table
  ## turso: A Turso object
  ## table: The name of the table
  ## fields: The fields to insert into
  ## values: The values to insert
  ## return: The ID of the inserted row
  ## Example:
  ## ```var id = turso.insertId("table", @["field1", "field2"], @["value1", "value2"])```
  var
    fieldsStr = fields.join(", ")
    valuesStr = values.map(proc(x: string): string = "'" & x & "'").join(", ")
    sql = "INSERT INTO " & table & " (" & fieldsStr & ") VALUES (" & valuesStr & ");"

  result = $(turso.getData(sql)["last_insert_rowid"])