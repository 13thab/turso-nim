# Package

version       = "0.1.2"
author        = "13thab"
description   = "A new awesome nimble client for libsql and turso"
license       = "BSD-3-Clause"
srcDir = "src"


# Dependencies
requires "nim >= 2.0.2"
