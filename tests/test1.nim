import unittest

import ../src/turso

test "connect":
  var
    url = "https://[databaseName]-[organizationName].turso.io"
    token = "token"

    turso = connect(url, token)

  check url & "/v2/pipeline" == turso.url
  check token == turso.token
